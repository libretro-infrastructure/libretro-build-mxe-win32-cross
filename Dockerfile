FROM ubuntu:bionic

ENV DEBIAN_FRONTEND="noninteractive"

ARG uid
ARG branch=master
ENV branch=$branch

ENV PACKAGES \
    build-essential \
    bash \
    ccache \
    gcc \
    g++ \
    gettext \
    git \
    gnupg \
    gnupg2 \
    make \
    cmake \
    nsis \
    python3-yaml \
    python3-sphinx \
    python3-setuptools \
    texinfo

RUN set -eux; \
    apt-get update; \
    apt-get -y install $PACKAGES; \
    useradd -d /developer -m developer; \
    chown -R developer:developer /developer

ENV MXE_PACKAGES \
    mxe-i686-w64-mingw32.shared-bzip2 \
    mxe-i686-w64-mingw32.shared-curl \
    mxe-i686-w64-mingw32.shared-gcc \
    mxe-i686-w64-mingw32.shared-glib \
    mxe-i686-w64-mingw32.shared-gmp \
    mxe-i686-w64-mingw32.shared-gnutls \
    mxe-i686-w64-mingw32.shared-libpng \
    mxe-i686-w64-mingw32.shared-nettle \
    mxe-i686-w64-mingw32.shared-nsis \
    mxe-i686-w64-mingw32.shared-pixman \
    mxe-i686-w64-mingw32.shared-pkgconf \
    mxe-i686-w64-mingw32.shared-dlfcn-win32 \
    mxe-i686-w64-mingw32.shared-libsndfile \
    mxe-i686-w64-mingw32.shared-sdl2 \
    mxe-i686-w64-mingw32.shared-zlib \
    mxe-i686-w64-mingw32.shared-libxml2 \
    mxe-i686-w64-mingw32.shared-freetype \
    mxe-i686-w64-mingw32.shared-fribidi \
    mxe-i686-w64-mingw32.shared-ffmpeg \
    mxe-i686-w64-mingw32.shared-qt5 \
    mxe-i686-w64-mingw32.shared-openssl \
    mxe-i686-w64-mingw32.static-bzip2 \
    mxe-i686-w64-mingw32.static-curl \
    mxe-i686-w64-mingw32.static-gcc \
    mxe-i686-w64-mingw32.static-glib \
    mxe-i686-w64-mingw32.static-gmp \
    mxe-i686-w64-mingw32.static-gnutls \
    mxe-i686-w64-mingw32.static-libpng \
    mxe-i686-w64-mingw32.static-nettle \
    mxe-i686-w64-mingw32.static-nsis \
    mxe-i686-w64-mingw32.static-pixman \
    mxe-i686-w64-mingw32.static-pkgconf \
    mxe-i686-w64-mingw32.static-dlfcn-win32 \
    mxe-i686-w64-mingw32.static-libsndfile \
    mxe-i686-w64-mingw32.static-sdl2 \
    mxe-i686-w64-mingw32.static-zlib \
    mxe-i686-w64-mingw32.static-libxml2 \
    mxe-i686-w64-mingw32.static-freetype \
    mxe-i686-w64-mingw32.static-fribidi \
    mxe-i686-w64-mingw32.static-ffmpeg \
    mxe-i686-w64-mingw32.static-qt5 \
    mxe-i686-w64-mingw32.static-openssl

RUN set -eux; \
    echo "deb http://pkg.mxe.cc/repos/apt bionic main" > /etc/apt/sources.list.d/mxeapt.list; \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C6BF758A33A3A276; \
    apt-get update; \
    apt-get install -y $MXE_PACKAGES; \
    apt-get -y autoremove; \
    apt-get -y clean; \
    rm -rf /var/lib/apt/lists/*

RUN echo "developer:developer" | chpasswd && adduser developer sudo

ENV HOME=/developer
ENV PATH=/usr/lib/mxe/usr/bin:$PATH

USER root
WORKDIR /developer
VOLUME /developer

CMD /bin/bash
